import { AppDataSource } from "./data-source"
import { User } from "./entity/User"
import { port } from './config'

import app from './app'

AppDataSource.initialize().then(async () => {    
    app.listen(port)

    // insert new users for test
    await AppDataSource.manager.save(AppDataSource.manager.create(User, { 
        firstName: "Timber",
        lastName: "Saw", 
        age: 27 
     }));
     await AppDataSource.manager.save(AppDataSource.manager.create(User, { 
        firstName: "Phantom", 
        lastName: "Assassin", 
        age: 24 
     })); 


    console.log(`Express server has started on port ${port}. Open http://localhost:${port}/users to see results`)

}).catch(error => console.log(error))
