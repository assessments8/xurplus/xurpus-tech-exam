import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Demo3DArray {
    public static void main(String[] args){
        // Activity 1: Dynamic 3D Array
        System.out.println("Activity 1 - 3D Array:");
        int num_1 = getValidInt("Please enter a positive number: ");
        int num_2 = getValidInt("Please enter a positive number: ");
        int num_3 = getValidInt("Please enter a positive number: ");
        array3D_1(num_1, num_2, num_3);

        // Activity 2: Count words in a sentence with limit
        System.out.println("\n");
        System.out.println("Activity 2 - String manipulation:");
        Scanner scanner = new Scanner(System.in);

        System.out.println("Create a paragraph:");
        String paragraph = scanner.nextLine();
        System.out.println("Received data: ");
        System.out.println(paragraph);

        int limit = getValidInt("Max word count: ");

        // split the paragraph into words
        String[] words = paragraph.trim().split(" ");

        // while no delimiter and less than limit, save every word to temp
        // once delimiter is found and less than limit, save the temp to sentences
        // once limit reached, exit
        // display sentences
        String temp = "";
        String sentences = "";
        for(int i = 0; i < words.length; i++){
            if(i < limit) {
                if(words[i].contains(".") || words[i].contains("?") || words[i].contains("!")){
                    sentences += temp + words[i];
                    temp = "";
                } else {
                    temp += words[i] + " ";
                }
            } else {
                break;
            }
        }
        System.out.println(sentences);
    }

static int getValidInt(String message){
        int num = 0;
    Scanner scanner = new Scanner(System.in);

    // Get input
    // Validate input

    do {
        System.out.print(message);
        while (!scanner.hasNextInt()) {
            System.out.println("That's not a number!");
            scanner.next();
        }
        num = scanner.nextInt();
    } while (num <= 0);
        return num;
}

static void array3D_1(int num_1, int num_2, int num_3) {
    int[][][] array3D = new int[num_1][num_2][num_3];

    for(int i = 0; i < num_1; i++) {
        for (int j = 0; j < num_2; j++) {
            for (int k = 0; k < num_3; k++) {
                array3D[i][j][k] = i*j*k;
            }
        }
    }

    System.out.print("[");
    for (int i = 0; i < num_1; i++) {
        System.out.print(" [ ");

        for (int j = 0; j < num_2; j++) {
            System.out.print("[");

            for (int k = 0; k < num_3; k++) {
                String delim = k < num_3 - 1 ? "," : "";
                System.out.printf("%d%s", array3D[i][j][k], delim);
            }

            System.out.print("] ");
        }
        System.out.print("] ");
    }
    System.out.print("]");
}
}