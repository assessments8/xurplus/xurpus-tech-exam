check installed versions
node -v
docker -v

install typeorm
npm i -g typeorm

initialize typeorm and express
typeorm init --database postgres --express

    --express auto generates basic orm routes

create a file docker-compose.yml
install postgres inside docker
configure accordingly

run docker to start the defined services
docker compose up -d

    -d will run the service in the background

to flush what has been save in postgres image run - docker compose down

add nodemon as dev dependency

add dev script in package.json to and use nodemon to run index.ts

redefine username and password in src/data-source.ts to match what has been defined in our docker service

to install jest as dev dependency:
nmp i --save-dev jest supertest @types/jest ts-jest

add script on package.json:
"test":"jest"

to run test:
npm run test
