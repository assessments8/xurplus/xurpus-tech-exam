import * as request from 'supertest'
import app from "../../src/app"
import { port } from "../../src/config"
import { AppDataSource } from "../../src/data-source"

import { User } from "../../src/entity/User"

let connection, server

const goodUser = {
    firstName: 'John', 
    lastName: 'Doe',
    age: 28
}

const noFirstName = {
    lastName: 'Doe',
    age: 28
}

const noLastName = {
    firstName: 'John', 
    age: 28
}

const noAge = {
    firstName: 'John', 
    lastName: 'Doe',
}

const negativeAge = {
    firstName: 'John', 
    lastName: 'Doe',
    age: -8
}

const nonIntAge = {
    firstName: 'John', 
    lastName: 'Doe',
    age: "abc"
}

// DB initializer

beforeEach(async() => {
    connection = await AppDataSource.initialize() //connect db
    await connection.synchronize(true) //clearing db
    server = app.listen(port)
    await AppDataSource.manager.save(AppDataSource.manager.create(User, { 
        firstName: "Timber",
        lastName: "Saw", 
        age: 27 
     }));
     await AppDataSource.manager.save(AppDataSource.manager.create(User, { 
        firstName: "Phantom", 
        lastName: "Assassin", 
        age: 24 
     })); 
})

afterEach(() => {
    connection.close()
    server.close()
})


// GET method tests

it('GET /users (will fetch all 2 users)', async() => {
    const response = await request(app).get('/users')
    // console.log(response.body)
    expect(response.statusCode).toBe(200)
    expect(response.body).toEqual([
        { 
            id: 1,
            firstName: "Timber",
            lastName: "Saw", 
            age: 27 
         },
         { 
            id:2,
            firstName: "Phantom", 
            lastName: "Assassin", 
            age: 24 
         }

    ])
})

it('GET /users/1 (will fetch user with id=1)', async() => {
    const response = await request(app).get('/users/1')
    // console.log(response.body)
    expect(response.statusCode).toBe(200)
    expect(response.body).toEqual(
        { 
            id: 1,
            firstName: "Timber",
            lastName: "Saw", 
            age: 27 
         }
    )
})

it('GET /users/10 (non existing id)', async() => {
    const response = await request(app).get('/users/10')
    // console.log(response.body)
    expect(response.statusCode).toBe(500)
    expect(response.body).toEqual(
        { message: 'User does not exist.' }
    )
})

it('GET /users/abc (non integer id)', async() => {
    const response = await request(app).get('/users/abc')
    // console.log(response.body)
    expect(response.statusCode).toBe(400)
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    expect(response.body.errors[0]).toEqual({ 
        location: 'params',
        msg: 'id must be an integer.',
        param: 'id',
        value: 'abc'
    })
})

it('POST /users (add new user)', async() => {
    const response = await request(app).post('/users').send(goodUser)    
    expect(response.statusCode).toBe(200)
    expect(response.body).toEqual({...goodUser, id: 3})
})

it('POST /users (no firstname user)', async() => {
    const response = await request(app).post('/users').send(noFirstName)    
    expect(response.statusCode).toBe(400)
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    // console.log(response.body.errors[0])
    expect(response.body.errors[0]).toEqual({ 
        msg: 'Invalid value', 
        param: 'firstName', 
        location: 'body' 
    })
   
})

it('POST /users (no lastname user)', async() => {
    const response = await request(app).post('/users').send(noLastName)    
    expect(response.statusCode).toBe(400)
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)    
    expect(response.body.errors[0]).toEqual({ 
        msg: 'Invalid value', 
        param: 'lastName', 
        location: 'body' 
    })
   
})

it('POST /users (no age user)', async() => {
    const response = await request(app).post('/users').send(noAge)    
    expect(response.statusCode).toBe(400)
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    expect(response.body.errors[0]).toEqual({ 
        msg: 'age is required and must be a positive integer.', 
        param: 'age', 
        location: 'body' 
    })
   
})

it('POST /users (user with negative age)', async() => {
    const response = await request(app).post('/users').send(negativeAge)    
    expect(response.statusCode).toBe(400)
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    // console.log(response.body.errors[0])
    expect(response.body.errors[0]).toEqual({
      value: -8,
      msg: 'age is required and must be a positive integer.',
      param: 'age',
      location: 'body'
    })
   
})

it('POST /users (user with non integer age)', async() => {
    const response = await request(app).post('/users').send(nonIntAge)    
    expect(response.statusCode).toBe(400)
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    // console.log(response.body.errors[0])
    expect(response.body.errors[0]).toEqual({
      value: "abc",
      msg: 'age is required and must be a positive integer.',
      param: 'age',
      location: 'body'
    })
   
})



// DEL Method test

it('DELETE /users/1', async () => {
    const response = await request(app).delete('/users/1')  
    expect(response.body).toEqual({message:"User successfully deleted"})
})    

it('DELETE /users/10', async () => {
    const response = await request(app).delete('/users/10')  
    expect(response.body).toEqual({message:"User does not exist."})
})

it('DELETE /users/abc', async () => {
    const response = await request(app).delete('/users/abc')  
    expect(response.statusCode).toBe(400)
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    expect(response.body.errors[0]).toEqual({
        value: 'abc',
        msg: 'id must be an integer.',
        param: 'id',
        location: 'params'
      })
})    


// PUT Method tests

it('PUT /users/abc (non integer id)', async () => {
    const response = await request(app).put('/users/abc')      
    expect(response.body.errors[0]).toEqual({
        value: 'abc',
        msg: 'id must be an integer.',
        param: 'id',
        location: 'params'
      })
})    

it('PUT /users/10 (non existing id)', async () => {
    const response = await request(app).put('/users/10')      
    expect(response.body).toEqual({message:"User does not exist."})
})

it('PUT /users (update firstName only)', async () => {
    const response = await request(app).put('/users/1').send({firstName:"Timber update"})
    expect(response.statusCode).toBe(200)
    expect(response.body)
   expect(response.body).toEqual({ id: 1, firstName: 'Timber update', lastName: 'Saw', age: 27 })
})

it('PUT /users (update lastName only)', async () => {
    const response = await request(app).put('/users/1').send({lastName:"Saw update"})
    expect(response.statusCode).toBe(200)
    expect(response.body)
   expect(response.body).toEqual({ id: 1, firstName: 'Timber', lastName: 'Saw update', age: 27 })
})

it('PUT /users (update age only)', async () => {
    const response = await request(app).put('/users/1').send({ age: 72 })
    expect(response.statusCode).toBe(200)
    expect(response.body)
   expect(response.body).toEqual({ id: 1, firstName: 'Timber', lastName: 'Saw', age: 72 })
})

it('PUT /users (non integer age)', async () => {
    const response = await request(app).put('/users/1').send({ age: "abc" })
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    expect(response.body.errors[0]).toEqual({
        value: 'abc',
        msg: 'age must be a positive integer.',
        param: 'age',
        location: 'body'
      })
})

it('PUT /users (negative age)', async () => {
    const response = await request(app).put('/users/1').send({ age: -25 })
    expect(response.body.errors).not.toBeNull()
    expect(response.body.errors.length).toBe(1)
    expect(response.body.errors[0]).toEqual({
        value: -25,
        msg: 'age must be a positive integer.',
        param: 'age',
        location: 'body'
      })
})

it('PUT /users (update all)', async () => {
    const response = await request(app).put('/users/1').send({firstName: 'Timber Update', lastName: 'Saw Update', age: 72})
    expect(response.statusCode).toBe(200)
    expect(response.body)
   expect(response.body).toEqual({ id:1, firstName: 'Timber Update', lastName: 'Saw Update', age: 72 })
})