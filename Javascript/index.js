const input = document.querySelector('#sentence')
const output = document.querySelector('#validSentence')
const wordCount = document.querySelector('#count')
const limit = document.querySelector('#limit')
const btnProcess = document.querySelector('.btn-process')
const btnClear = document.querySelector('.btn-clear')

function countWords() {
  output.value = ''
  const wordValue = input.value

  const lines = wordValue
    .trim()
    .split(/\r?\n/)
    .map((word) => word.split(' '))
    .flat()

  let temp = ''
  let count = 1
  for (let i = 0; i < lines.length; i++) {
    if (i < parseInt(limit.value)) {
      if (
        lines[i].includes('.') ||
        lines[i].includes('?') ||
        lines[i].includes('!')
      ) {
        output.value += temp + lines[i] + '\n'
        wordCount.value = count++
        temp = ''
      } else {
        temp += lines[i] + ' '
      }
    } else {
      break
    }
  }
}

function reset() {
  input.value = ''
  wordCount.value = ''
  output.value = ''
}

btnProcess.addEventListener('click', countWords)
output.addEventListener('onchange', countWords)
btnClear.addEventListener('click', reset)

// multidimensional array
const num1 = document.querySelector('#num1')
const num2 = document.querySelector('#num2')
const num3 = document.querySelector('#num3')
const result = document.querySelector('#ta-arr-output')
const btnReset = document.querySelector('.arr-reset')

function getArrayIndexProduct() {
  result.value = ''
  let re = ''
  re += '[ '
  for (let x = 0; x < parseInt(num1.value); x++) {
    re += ' [ '
    for (let y = 0; y < parseInt(num2.value); y++) {
      re += '['
      for (let z = 0; z < parseInt(num3.value); z++) {
        re += (x * y * z).toString()
      }
      re += ']'
    }
    re += ' ] '
  }
  re += ' ]'

  console.log(re)
  result.value = re
}

num1.addEventListener('input', getArrayIndexProduct)
num2.addEventListener('input', getArrayIndexProduct)
num3.addEventListener('input', getArrayIndexProduct)

btnReset.addEventListener('click', () => {
  result.value = ''
  num1.value = 2
  num2.value = 2
  num3.value = 2
  getArrayIndexProduct()
})

window.onload = getArrayIndexProduct
