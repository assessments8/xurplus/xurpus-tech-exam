-- -- create database
-- CREATE DATABASE IF NOT EXISTS xurpas_books_db;

-- -- use database created
-- USE xurpas_books_db;

-- -- create author table
-- CREATE TABLE IF NOT EXISTS authors (
--     author_name VARCHAR(50),
--     book_name VARCHAR(50)	
-- );
-- insert into authors (author_name, book_name) values ('author_1', 'book_1');
-- insert into authors (author_name, book_name) values ('author_1', 'book_2');
-- insert into authors (author_name, book_name) values ('author_2', 'book_3');
-- insert into authors (author_name, book_name) values ('author_2', 'book_4');
-- insert into authors (author_name, book_name) values ('author_3', 'book_5');
-- insert into authors (author_name, book_name) values ('author_3', 'book_6');


-- -- create book table
-- CREATE TABLE IF NOT EXISTS books (	
-- 	book_name VARCHAR(50),
--  sold_copies DECIMAL
-- );
-- insert into books (book_name, sold_copies) values ('book_1', 1000);
-- insert into books (book_name, sold_copies) values ('book_2', 1500);
-- insert into books (book_name, sold_copies) values ('book_3', 34000);
-- insert into books (book_name, sold_copies) values ('book_4', 29000);
-- insert into books (book_name, sold_copies) values ('book_5', 40000);
-- insert into books (book_name, sold_copies) values ('book_6', 4400);

-- SQL Question #1
-- Create an SQL Query that shows the TOP 3 authors who sold most books in total
-- SELECT T1.author_name, SUM(T2.sold_copies) AS total_sold
-- FROM authors AS T1
-- LEFT JOIN books AS T2
-- ON T1.book_name = T2.book_name
-- GROUP BY T1.author_name
-- ORDER BY sold DESC
-- LIMIT 3;


-- SQL Question #2
-- SELECT COUNT(*) 
-- FROM (
--     SELECT user_id, COUNT(event_date_time) AS insertion_count
-- 	FROM event_log
-- 	GROUP BY user_id
-- ) AS derived_table
-- WHERE derived_table.insertion_count < 2000 AND derived_table.insertion_count > 1000;


-- SQL Question #3
-- create book table
-- CREATE TABLE IF NOT EXISTS employees (	
-- 	department_name VARCHAR(50) NOT NULL,
--     employee_id int UNIQUE, 
--     employee_name VARCHAR(50)
-- );
-- insert into employees (department_name, employee_id, employee_name) values ('Sales', 123, 'John Doe');
-- insert into employees (department_name, employee_id, employee_name) values ('Sales', 211, 'Jane Smith');
-- insert into employees (department_name, employee_id, employee_name) values ('HR', 556, 'Billy Bob');
-- insert into employees (department_name, employee_id, employee_name) values ('Sales', 711, 'Robert Hayek');
-- insert into employees (department_name, employee_id, employee_name) values ('Marketing', 235, 'Edward Jorgson');
-- insert into employees (department_name, employee_id, employee_name) values ('Marketing', 236, 'Christine Packard');

-- CREATE TABLE IF NOT EXISTS salaries (
--     salary DECIMAL,
--     employee_id int UNIQUE,
--     employee_name VARCHAR(50)
-- );
-- insert into salaries (salary, employee_id, employee_name) values (500, 123, 'John Doe');
-- insert into salaries (salary, employee_id, employee_name) values (600, 211, 'Jane Smith');
-- insert into salaries (salary, employee_id, employee_name) values (1000, 556, 'Billy Bob');
-- insert into salaries (salary, employee_id, employee_name) values (400, 711, 'Robert Hayek');
-- insert into salaries (salary, employee_id, employee_name) values (1200, 235, 'Edward Jorgson');
-- insert into salaries (salary, employee_id, employee_name) values (200, 236, 'Christine Packard');

-- SELECT T1.department_name, ROUND(AVG(T2.salary),2) AS avg_salary
-- FROM employees AS T1
-- LEFT JOIN salaries AS T2
-- ON T1.employee_id = T2.employee_id
-- GROUP BY T1.department_name
-- HAVING avg_salary < 500;