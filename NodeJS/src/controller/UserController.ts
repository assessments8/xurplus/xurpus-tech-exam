import { getRepository } from "typeorm"
import { NextFunction, Request, Response } from "express"
import { User } from "../entity/User"
import {AppDataSource} from "../data-source"

export class UserController {

    private userRepository = AppDataSource.getRepository(User)

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find()
    }

    async one(request: Request, response: Response, next: NextFunction) {
        const user = await this.userRepository.findOneBy({id: request.params.id})        
        
        if(user != null) return user
        throw Error('User does not exist.')        
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return await this.userRepository.save(request.body)
    }

    async update(request: Request, response: Response, next: NextFunction) {
        const userToUpdate = await this.userRepository.findOneBy({ id: request.params.id })
        
        if(!userToUpdate) throw Error('User does not exist.')

        this.userRepository.merge(userToUpdate, request.body)
        const result = await this.userRepository.save(userToUpdate)
        return result
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        const userToRemove = await this.userRepository.findOneBy({ id: request.params.id })
        
        if(!userToRemove) throw Error('User does not exist.')
        
        await this.userRepository.remove(userToRemove)
        
        response.status(200).json({message:"User successfully deleted"})
    }

}